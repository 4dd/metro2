((Drupal, once) => {
  'use strict';

  Drupal.behaviors.osmMapMskFunctions = {
    attach: (context, settings) => {

      // Add the PMTiles plugin to the maplibregl global.
      const protocol = new pmtiles.Protocol();
      maplibregl.addProtocol('pmtiles', protocol.tile);

      const pmtilesUrl = 'https://cdn.4dd.pw/metro2/pmtiles/m2o-msk.pmtiles';
      const tiles = new pmtiles.PMTiles(pmtilesUrl);
      protocol.add(tiles);

      const metroSource = {
        'metro': {
          type: 'geojson',
          data: 'https://cdn.4dd.pw/metro2/maps/m2msk.geojson',
        },
      };

      const styles= getStyles(pmtilesUrl, metroSource);

      // Defining initial position.
      const dsLat = settings.geolocationJSValuesFormatter?.lat;
      const dsLng = settings.geolocationJSValuesFormatter?.lng;
      const lat = (typeof dsLat === 'undefined' || dsLat === 0) ? 55.7332389 : dsLat;
      const lng = (typeof dsLng === 'undefined' || dsLng === 0) ? 37.6043246 : dsLng;
      const zoom = (lat === 55.7332389 && lng === 37.6043246) ? 11 : 17;

      once('osmMapMskBehavior', 'html').forEach(element => {
        let defaultStyle;
        if (localStorage.getItem('map-style') !== 'hybrid') {
          localStorage.setItem('map-style', 'map');
          defaultStyle = styles.map;
        }
        else {
          defaultStyle = styles.hybrid;
        }

        // Initializing map.
        const map = new maplibregl.Map({
          container: 'gmap-msk',
          zoom: zoom,
          center: [lng, lat],
          style: defaultStyle,
          maxBounds: [35.403528, 55.109138, 39.809046, 56.391983],
        });

        MapLibreStyleSwitcherControl.DEFAULT_STYLE = styles.map;
        MapLibreStyleSwitcherControl.DEFAULT_STYLES = styles;

        map.on('load', () => {
          map.addControl(new maplibregl.NavigationControl());
          map.addControl(new MapLibreStyleSwitcherControl(styles, defaultStyle));

          map.on('click', 'metro-objects', e => showDescriptionPopup(e, map));
          map.on('click', 'metro-lines', e => showDescriptionPopup(e, map));

          map.on('mouseenter', 'metro-objects', () => map.getCanvas().style.cursor = 'pointer');
          map.on('mouseleave', 'metro-objects', () => map.getCanvas().style.cursor = '');

          map.on('mouseenter', 'metro-lines', () => map.getCanvas().style.cursor = 'pointer');
          map.on('mouseleave', 'metro-lines', () => map.getCanvas().style.cursor = '');
        });
      });

    }
  };
}) (Drupal, once);
