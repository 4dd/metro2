((Drupal, once) => {
  'use strict';

  Drupal.behaviors.osmMapSpbFunctions = {
    attach: (context, settings) => {

      // Add the PMTiles plugin to the maplibregl global.
      const protocol = new pmtiles.Protocol();
      maplibregl.addProtocol('pmtiles', protocol.tile);

      const pmtilesUrl = 'https://cdn.4dd.pw/metro2/pmtiles/m2o-spb.pmtiles';
      const tiles = new pmtiles.PMTiles(pmtilesUrl);
      protocol.add(tiles);

      const metroSource = {
        'metro': {
          type: 'geojson',
          data: 'https://cdn.4dd.pw/metro2/maps/m2spb.geojson',
        },
      };

      const styles= getStyles(pmtilesUrl, metroSource);

      // Defining initial position.
      const dsLat = settings.geolocationJSValuesFormatter?.lat;
      const dsLng = settings.geolocationJSValuesFormatter?.lng;
      const lat = (typeof dsLat === 'undefined' || dsLat === 0) ? 59.9384808 : dsLat;
      const lng = (typeof dsLng === 'undefined' || dsLng === 0) ? 30.325042: dsLng;
      const zoom = (lat === 59.9384808 && lng === 30.325042) ? 11 : 17;

      once('osmMapSpbBehavior', 'html').forEach(element => {
        let defaultStyle;
        if (localStorage.getItem('map-style') !== 'hybrid') {
          localStorage.setItem('map-style', 'map');
          defaultStyle = styles.map;
        }
        else {
          defaultStyle = styles.hybrid;
        }

        // Initializing map.
        const map = new maplibregl.Map({
          container: 'gmap-spb',
          zoom: zoom,
          center: [lng, lat],
          style: defaultStyle,
          maxBounds: [29.400283, 59.684284, 31.293107, 60.180053],
        });

        MapLibreStyleSwitcherControl.DEFAULT_STYLE = styles.map;
        MapLibreStyleSwitcherControl.DEFAULT_STYLES = styles;

        map.on('load', () => {
          map.addControl(new maplibregl.NavigationControl());
          map.addControl(new MapLibreStyleSwitcherControl(styles, defaultStyle));

          map.on('click', 'metro-objects', e => showDescriptionPopup(e, map));
          map.on('click', 'metro-lines', e => showDescriptionPopup(e, map));

          map.on('mouseenter', 'metro-objects', () => map.getCanvas().style.cursor = 'pointer');
          map.on('mouseleave', 'metro-objects', () => map.getCanvas().style.cursor = '');

          map.on('mouseenter', 'metro-lines', () => map.getCanvas().style.cursor = 'pointer');
          map.on('mouseleave', 'metro-lines', () => map.getCanvas().style.cursor = '');
        });
      });

    }
  };
}) (Drupal, once);
