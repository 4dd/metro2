(g=>{var h,a,k,p="The Google Maps JavaScript API",c="google",l="importLibrary",q="__ib__",m=document,b=window;b=b[c]||(b[c]={});var d=b.maps||(b.maps={}),r=new Set,e=new URLSearchParams,u=()=>h||(h=new Promise(async(f,n)=>{await (a=m.createElement("script"));e.set("libraries",[...r]+"");for(k in g)e.set(k.replace(/[A-Z]/g,t=>"_"+t[0].toLowerCase()),g[k]);e.set("callback",c+".maps."+q);a.src=`https://maps.${c}apis.com/maps/api/js?`+e;d[q]=f;a.onerror=()=>h=n(Error(p+" could not load."));a.nonce=m.querySelector("script[nonce]")?.nonce||"";m.head.append(a)}));d[l]?console.warn(p+" only loads once. Ignoring:",g):d[l]=(f,...n)=>r.add(f)&&u().then(()=>d[l](f,...n))})({
  key: "AIzaSyBhCmKnvw7iOCq5OC8CmpJ2Y5xHrIOhmdg",
  v: "weekly",
  // Use the 'v' parameter to indicate the version to use (weekly, beta, alpha, etc.).
  // Add other bootstrap parameters as needed, using camel case.
});

let map;

async function initMap() {
  // Dark map style.
  const featureOpts = [
    {
      'elementType': 'labels.icon',
      'stylers': [
        {
          'visibility': 'off',
        },
      ],
    },
    {
      'elementType': 'labels.text.fill',
      'stylers': [
        {
          'color': '#8e8e8e',
        },
      ],
    },
    {
      'elementType': 'labels.text.stroke',
      'stylers': [
        {
          'color': '#000',
        },
        {
          'lightness': 15,
        },
        {
          'visibility': '0ff',
        },
      ],
    },
    {
      'featureType': 'administrative.locality',
      'elementType': 'labels.text',
      'stylers': [
        {
          'visibility': 'off',
        },
      ],
    },
    {
      'featureType': 'administrative.neighborhood',
      'elementType': 'labels.text',
      'stylers': [
        {
          'visibility': 'off',
        },
      ],
    },
    {
      'featureType': 'administrative.province',
      'elementType': 'labels.text',
      'stylers': [
        {
          'visibility': 'off',
        },
      ],
    },
    {
      'featureType': 'administrative',
      'elementType': 'geometry.fill',
      'stylers': [
        {
          'color': '#000',
        },
        {
          'lightness': 15,
        },
      ],
    },
    {
      'featureType': 'administrative',
      'elementType': 'geometry.stroke',
      'stylers': [
        {
          'color': '#000',
        },
        {
          'lightness': 15,
        },
        {
          'weight': 1,
        },
      ],
    },
    {
      'featureType': 'landscape',
      'elementType': 'geometry',
      'stylers': [
        {
          'color': '#000',
        },
        {
          'lightness': 20,
        },
      ],
    },
    {
      'featureType': 'landscape',
      'elementType': 'geometry.fill',
      'stylers': [
        {
          'color': '#191e1e',
        },
      ],
    },
    {
      'featureType': 'landscape.man_made',
      'elementType': 'geometry.stroke',
      'stylers': [
        {
          'color': '#414954',
        },
      ],
    },
    {
      'featureType': 'landscape.natural',
      'elementType': 'labels.text',
      'stylers': [
        {
          'color': '#767676',
        },
      ],
    },
    {
      'featureType': 'landscape.natural',
      'elementType': 'labels.text.stroke',
      'stylers': [
        {
          'color': '#2e2e2e',
        },
      ],
    },
    {
      'featureType': 'poi',
      'elementType': 'geometry',
      'stylers': [
        {
          'color': '#1c2020',
        },
        {
          'visibility': 'simplified',
        },
      ],
    },
    {
      'featureType': 'poi.park',
      'elementType': 'geometry.fill',
      'stylers': [
        {
          'color': '#14201b',
        },
      ],
    },
    {
      'featureType': 'poi.park',
      'elementType': 'labels.text',
      'stylers': [
        {
          'color': '#436846',
        },
      ],
    },
    {
      'featureType': 'poi.park',
      'elementType': 'labels.text.stroke',
      'stylers': [
        {
          'color': '#162217',
        },
      ],
    },
    {
      'featureType': 'road',
      'elementType': 'labels.text.fill',
      'stylers': [
        {
          'color': '#7a7c80',
        },
      ],
    },
    {
      'featureType': 'road.arterial',
      'elementType': 'geometry',
      'stylers': [
        {
          'color': '#000',
        },
        {
          'lightness': 18,
        },
      ],
    },
    {
      'featureType': 'road.arterial',
      'elementType': 'geometry.fill',
      'stylers': [
        {
          'color': '#1f2323',
        },
      ],
    },
    {
      'featureType': 'road.arterial',
      'elementType': 'geometry.stroke',
      'stylers': [
        {
          'color': '#202022',
        },
      ],
    },
    {
      'featureType': 'road.highway',
      'elementType': 'geometry.fill',
      'stylers': [
        {
          'color': '#232019',
        },
        {
          'lightness': 5,
        },
      ],
    },
    {
      'featureType': 'road.highway',
      'elementType': 'geometry.stroke',
      'stylers': [
        {
          'color': '#202022',
        },
        {
          'lightness': 29,
        },
        {
          'weight': 0.2,
        },
      ],
    },
    {
      'featureType': 'road.local',
      'elementType': 'geometry',
      'stylers': [
        {
          'color': '#000',
        },
        {
          'lightness': 16,
        },
      ],
    },
    {
      'featureType': 'road.local',
      'elementType': 'geometry.fill',
      'stylers': [
        {
          'color': '#1f2323',
        },
      ],
    },
    {
      'featureType': 'road.local',
      'elementType': 'geometry.stroke',
      'stylers': [
        {
          'color': '#202022',
        },
      ],
    },
    {
      'featureType': 'transit',
      'elementType': 'geometry',
      'stylers': [
        {
          'color': '#000',
        },
        {
          'lightness': 14,
        },
      ],
    },
    {
      'featureType': 'water',
      'elementType': 'geometry',
      'stylers': [
        {
          'color': '#000',
        },
        {
          'lightness': 17,
        },
      ],
    },
    {
      'featureType': 'water',
      'elementType': 'geometry.fill',
      'stylers': [
        {
          'color': '#132529',
        },
      ],
    },
    {
      'featureType': 'water',
      'elementType': 'labels.text',
      'stylers': [
        {
          'color': '#2e6681',
        },
      ],
    },
  ];

  // Defining initial position.
  const dsLat = drupalSettings.geolocationJSValuesFormatter?.lat;
  const dsLng = drupalSettings.geolocationJSValuesFormatter?.lng;
  const lat = (typeof dsLat === 'undefined' || dsLat === 0) ? 59.9384808 : dsLat;
  const lng = (typeof dsLng === 'undefined' || dsLng === 0) ? 30.325042: dsLng;
  const zoom = (lat === 59.9384808 && lng === 30.325042) ? 11 : 17;

  // Creating map object.
  const { Map } = await google.maps.importLibrary('maps');
  map = new Map(document.getElementById('gmap-spb'), {
    zoom: zoom,
    center: { lat: lat, lng: lng },
    gestureHandling: 'greedy',
    backgroundColor: 'none',
    disableDefaultUI: true,
    zoomControl: true,
    zoomControlOptions: {
      position: google.maps.ControlPosition.RIGHT_CENTER,
    },
    mapTypeControl: true,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
      position: google.maps.ControlPosition.RIGHT_TOP,
      mapTypeIds: ['map_style', 'hybrid'],
    },
  });

  // Applying dark map style.
  const styledMap = new google.maps.StyledMapType(featureOpts, { name: 'Map' });
  map.mapTypes.set('map_style', styledMap);
  map.setMapTypeId('map_style');

  // Applying data layer.
  map.data.loadGeoJson('https://cdn.4dd.pw/metro2/maps/m2spb.geojson');
  map.data.setStyle((feature) => {
    const featureType = feature.getGeometry().getType();
    const stroke = feature.getProperty('stroke');
    const properties = {
      strokeColor: stroke,
      strokeWeight: feature.getProperty('stroke-width') || 0.5,
      strokeOpacity: Math.round(feature.getProperty('stroke-opacity') * 100) / 100,
    };

    if (featureType === 'LineString') {
      if (stroke === '#161616') {
        // Service lines.
        properties['zIndex'] = -2;
      } else if (stroke === '#20ffff') {
        // Land path.
        properties['zIndex'] = 1;
      } else {
        // Regular lines.
        properties['zIndex'] = -1;
      }
    } else if (featureType === 'Polygon'){
      const fill = feature.getProperty('fill');
      properties['fillColor'] = fill;
      properties['fillOpacity'] = Math.round(feature.getProperty('fill-opacity') * 100) / 100;
      if (fill === '#aaaaaa') {
        // Bridges.
        properties['zIndex'] = -3;
      }
    }

    return properties;
  });

  // Showing info window when user clicks on object with description.
  const infoWindow = new google.maps.InfoWindow({
    content: '',
  });
  map.data.addListener('click', (event) => {
    const description = event.feature.getProperty('description');
    if (typeof description === 'undefined' || !description.value) {
      return;
    }

    infoWindow.setContent(description.value);
    infoWindow.setPosition(event.latLng);
    infoWindow.open(map);
  });

}

initMap();
