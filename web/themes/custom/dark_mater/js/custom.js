((Drupal, once) => {
  'use strict';

  Drupal.behaviors.darkMatterFunctions = {
    attach: (context, settings) => {

      // Main content toggle.
      const toggleMainContent = (event) => {
        event.stopPropagation();
        event.preventDefault();
        document.body.classList.toggle('main-content-open');
      };

      // Table rows.
      const handleRowClick = (element) => {
        const anchor = element.querySelector('.views-field-title a');
        if (anchor) {
          window.location = anchor.href;
        }
      };

      const handleLineStationsClasses = (station) => {
        if (station.classList.contains('station-inactive')) {
          const prevStation = station.previousElementSibling;
          if (prevStation) {
            prevStation.classList.add('station-inactive-line');

            // Check for active prev and next stations.
            const nextStation = station.nextElementSibling;
            if (nextStation &&
              !prevStation.classList.contains('station-inactive') &&
              !nextStation.classList.contains('station-inactive')) {
              station.classList.add('station-active-line');
              prevStation.classList.remove('station-inactive-line');
            }
          }
        }

        if (station.classList.contains('station-closed') && station.previousElementSibling) {
          station.previousElementSibling.classList.add('station-no-line', 'station-end-last');
        }

        if (station.classList.contains('station-alter-prev') &&
          !station.classList.contains('station-fork-reverse') &&
          station.previousElementSibling) {
          station.previousElementSibling.classList.add('station-no-line', 'station-end-last');
        }
      };

      once('globalBehaviors', 'html').forEach(element => {
        if (window.innerWidth > 992) {
          document.body.classList.add('main-content-open');
        }

        const contentToggle = document.getElementById('main-content-toggle');
        if (contentToggle) {
          contentToggle.addEventListener('click', toggleMainContent);
        }

        const contentToggleFab = document.getElementById('main-content-toggle-fab');
        if (contentToggleFab) {
          contentToggleFab.addEventListener('click', toggleMainContent);
        }

        // Share toggle.
        const shareToggleFab = document.getElementById('share-toggle-fab');
        if (shareToggleFab) {
          shareToggleFab.addEventListener('click', event => {
            event.stopPropagation();
            event.preventDefault();
          });
        }
      });

      once('tableRowClick', '.table-list tbody tr', context).forEach(element => {
        element.addEventListener('click', () => handleRowClick(element));
      });

      once('cardResultClick', '.card-result', context).forEach(element => {
        element.addEventListener('click', () => handleRowClick(element));
      });

      // Handling line stations classes.
      once('lineStationClasses', '.line-stations .station', context).forEach(station => handleLineStationsClasses(station));

      once('messageCloseClick', '.messages-close', context).forEach(element => {
        element.addEventListener('click', () => slideUp(element.parentElement, 200));
      });

      once('messageAutoHide', '.messages', context).forEach(element => {
        setTimeout(() => slideUp(element, 200), 5000);
      });

    }
  };
}) (Drupal, once);
