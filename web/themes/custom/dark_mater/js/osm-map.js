const metroLayers = [
  {
    'id': 'metro-lines',
    'type': 'line',
    'source': 'metro',
    'layout': {
      'line-join': 'round',
      'line-cap': 'round',
    },
    'paint': {
      'line-color': ['get', 'stroke'],
      'line-width': ['get', 'stroke-width'],
      'line-opacity': ['get', 'stroke-opacity'],
    },
    'filter': ['!=', '$type', 'Polygon'],
  },
  {
    'id': 'metro-objects',
    'type': 'fill',
    'source': 'metro',
    'paint': {
      'fill-color': ['get', 'fill'],
      'fill-opacity': ['get', 'fill-opacity'],
      'fill-outline-color': 'rgba(0,0,0,0.5)',
    },
    'filter': ['==', '$type', 'Polygon'],
  },
];

const getStyles = (pmtilesUrl, metroSource) => ({
  map: {
    version: 8,
    name: 'Map',
    sources: {
      'protomaps': {
        type: 'vector',
        url: 'pmtiles://' + pmtilesUrl,
        attribution: '<a href="https://protomaps.com/">© Protomaps</a> | <a href="https://openstreetmap.org">© OpenStreetMap</a>',
      },
      ...metroSource,
    },
    glyphs: 'https://protomaps.github.io/basemaps-assets/fonts/{fontstack}/{range}.pbf',
    layers: [
      {
        'id': 'background',
        'type': 'background',
        'paint': {
          'background-color': '#181c1c', // "#34373d"
        },
      },
      {
        'id': 'earth',
        'type': 'fill',
        'source': 'protomaps',
        'source-layer': 'earth',
        'paint': {
          'fill-color': '#181c1c', //"#1f1f1f"
        },
      },
      {
        'id': 'landuse_park',
        'type': 'fill',
        'source': 'protomaps',
        'source-layer': 'landuse',
        'filter': ['any', ['in', 'pmap:kind', 'national_park', 'park', 'cemetery', 'protected_area', 'nature_reserve', 'forest', 'golf_course']],
        'paint': {
          'fill-color': ['interpolate', ['linear'], ['zoom'],
            0, '#181c1c', //"#232325",
            12, '#181c1c', //"#232325"
          ],
        },
      },
      {
        'id': 'landuse_urban_green',
        'type': 'fill',
        'source': 'protomaps',
        'source-layer': 'landuse',
        'filter': ['any', ['in', 'pmap:kind', 'allotments', 'village_green', 'playground']],
        'paint': {
          'fill-color': '#162217', //"#232325",
          'fill-opacity': 0.7,
        },
      },
      {
        'id': 'landuse_hospital',
        'type': 'fill',
        'source': 'protomaps',
        'source-layer': 'landuse',
        'filter': ['any', ['==', 'pmap:kind', 'hospital']],
        'paint': {
          'fill-color': '#252424',
        },
      },
      {
        'id': 'landuse_industrial',
        'type': 'fill',
        'source': 'protomaps',
        'source-layer': 'landuse',
        'filter': ['any', ['==', 'pmap:kind', 'industrial']],
        'paint': {
          'fill-color': '#181c1c', //"#222222"
        },
      },
      {
        'id': 'landuse_school',
        'type': 'fill',
        'source': 'protomaps',
        'source-layer': 'landuse',
        'filter': ['any', ['in', 'pmap:kind', 'school', 'university', 'college']],
        'paint': {
          'fill-color': '#262323',
        },
      },
      {
        'id': 'landuse_beach',
        'type': 'fill',
        'source': 'protomaps',
        'source-layer': 'landuse',
        'filter': ['any', ['in', 'pmap:kind', 'beach']],
        'paint': {
          'fill-color': '#28282a',
        },
      },
      {
        'id': 'landuse_zoo',
        'type': 'fill',
        'source': 'protomaps',
        'source-layer': 'landuse',
        'filter': ['any', ['in', 'pmap:kind', 'zoo']],
        'paint': {
          'fill-color': '#222323',
        },
      },
      {
        'id': 'landuse_military',
        'type': 'fill',
        'source': 'protomaps',
        'source-layer': 'landuse',
        'filter': ['any', ['in', 'pmap:kind', 'military', 'naval_base', 'airfield']],
        'paint': {
          'fill-color': '#222323',
        },
      },
      {
        'id': 'natural_wood',
        'type': 'fill',
        'source': 'protomaps',
        'source-layer': 'natural',
        'filter': ['any', ['in', 'pmap:kind', 'wood', 'nature_reserve', 'forest']],
        'paint': {
          'fill-color': ['interpolate', ['linear'], ['zoom'],
            0, '#162217', // "#202121",
            12, '#162217', //"#202121"
          ],
        },
      },
      {
        'id': 'natural_scrub',
        'type': 'fill',
        'source': 'protomaps',
        'source-layer': 'natural',
        'filter': [
          'in',
          'pmap:kind',
          'scrub',
          'grassland',
          'grass',
        ],
        'paint': {
          'fill-color': ['interpolate', ['linear'], ['zoom'],
            0, '#162217', //"#222323",
            12, '#162217', //"#222323"
          ],
        },
      },
      {
        'id': 'natural_glacier',
        'type': 'fill',
        'source': 'protomaps',
        'source-layer': 'natural',
        'filter': ['==', 'pmap:kind',
          'glacier',
        ],
        'paint': {
          'fill-color': '#1c1c1c',
        },
      },
      {
        'id': 'natural_sand',
        'type': 'fill',
        'source': 'protomaps',
        'source-layer': 'natural',
        'filter': ['==', 'pmap:kind', 'sand'],
        'paint': {
          'fill-color': '#212123',
        },
      },
      {
        'id': 'landuse_aerodrome',
        'type': 'fill',
        'source': 'protomaps',
        'source-layer': 'landuse',
        'filter': ['any', ['in', 'pmap:kind', 'aerodrome']],
        'paint': {
          'fill-color': '#1e1e1e',
        },
      },
      {
        'id': 'transit_runway',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'transit',
        'filter': ['any', ['in', 'pmap:kind_detail', 'runway']],
        'paint': {
          'line-color': '#333',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 10, 0, 12, 4, 18, 30],
        },
      },
      {
        'id': 'transit_taxiway',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'transit',
        'minzoom': 13,
        'filter': ['any', ['in', 'pmap:kind_detail', 'taxiway']],
        'paint': {
          'line-color': '#333',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 13, 0, 13.5, 1, 15, 6],
        },
      },
      {
        'id': 'water',
        'type': 'fill',
        'source': 'protomaps',
        'source-layer': 'water',
        'paint': {
          'fill-color': '#132529', // "#34373d"
        },
      },
      {
        'id': 'physical_line_stream',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'physical_line',
        'minzoom': 14,
        'filter': ['all', ['in', 'pmap:kind', 'stream']],
        'paint': {
          'line-color': '#132529', //"#34373d",
          'line-width': 0.5,
        },
      },
      {
        'id': 'physical_line_river',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'physical_line',
        'minzoom': 9,
        'filter': ['all', ['in', 'pmap:kind', 'river']],
        'paint': {
          'line-color': '#132529', //"#34373d",
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 9, 0, 9.5, 1, 18, 12],
        },
      },
      {
        'id': 'landuse_pedestrian',
        'type': 'fill',
        'source': 'protomaps',
        'source-layer': 'landuse',
        'filter': ['any', ['==', 'pmap:kind', 'pedestrian']],
        'paint': {
          'fill-color': '#1e1e1e',
        },
      },
      {
        'id': 'landuse_pier',
        'type': 'fill',
        'source': 'protomaps',
        'source-layer': 'landuse',
        'filter': ['any', ['==', 'pmap:kind', 'pier']],
        'paint': {
          'fill-color': '#222',
        },
      },
      {
        'id': 'roads_tunnels_other_casing',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['<', 'pmap:level', 0], ['in', 'pmap:kind', 'other', 'path']],
        'paint': {
          'line-color': '#141414',
          'line-gap-width': ['interpolate', ['exponential', 1.6], ['zoom'], 14, 0, 20, 7],
        },
      },
      {
        'id': 'roads_tunnels_minor_casing',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['<', 'pmap:level', 0], ['==', 'pmap:kind', 'minor_road']],
        'paint': {
          'line-color': '#141414',
          'line-dasharray': [3,  2],
          'line-gap-width': ['interpolate', ['exponential', 1.6], ['zoom'], 11, 0, 12.5, 0.5, 15, 2, 18, 11],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 12, 0, 12.5, 1],
        },
      },
      {
        'id': 'roads_tunnels_link_casing',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['<', 'pmap:level', 0], ['==', 'pmap:link', 1]],
        'paint': {
          'line-color': '#141414',
          'line-dasharray': [3,  2],
          'line-gap-width': ['interpolate', ['exponential', 1.6], ['zoom'], 13, 0, 13.5, 1, 18, 11],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 12, 0, 12.5, 1],
        },
      },
      {
        'id': 'roads_tunnels_medium_casing',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['<', 'pmap:level', 0], ['==', 'pmap:kind', 'medium_road']],
        'paint': {
          'line-color': '#141414',
          'line-dasharray': [3,  2],
          'line-gap-width': ['interpolate', ['exponential', 1.6], ['zoom'], 7, 0, 7.5, 0.5, 18, 13],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 10, 0, 10.5, 1],
        },
      },
      {
        'id': 'roads_tunnels_major_casing',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['<', 'pmap:level', 0], ['==', 'pmap:kind', 'major_road']],
        'paint': {
          'line-color': '#141414',
          'line-dasharray': [3,  2],
          'line-gap-width': ['interpolate', ['exponential', 1.6], ['zoom'], 7, 0, 7.5, 0.5, 18, 13],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 9, 0, 9.5, 1],
        },
      },
      {
        'id': 'roads_tunnels_highway_casing',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['<', 'pmap:level', 0], ['==', 'pmap:kind', 'highway'], ['!=', 'pmap:link', 1]],
        'paint': {
          'line-color': '#141414',
          'line-dasharray': [6, 0.5],
          'line-gap-width': ['interpolate', ['exponential', 1.6], ['zoom'], 3, 0, 3.5, 0.5, 18, 15],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 7, 0, 7.5, 1, 20, 15],
        },
      },
      {
        'id': 'roads_tunnels_other',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['<', 'pmap:level', 0], ['in', 'pmap:kind', 'other', 'path']],
        'paint': {
          'line-color': '#292929',
          'line-dasharray': [4.5, 0.5],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 14, 0, 20, 7],
        },
      },
      {
        'id': 'roads_tunnels_minor',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['<', 'pmap:level', 0], ['==', 'pmap:kind', 'minor_road']],
        'paint': {
          'line-color': '#292929',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 11, 0, 12.5, 0.5, 15, 2, 18, 11],
        },
      },
      {
        'id': 'roads_tunnels_link',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['<', 'pmap:level', 0], ['==', 'pmap:link', 1]],
        'paint': {
          'line-color': '#292929',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 13, 0, 13.5, 1, 18, 11],
        },
      },
      {
        'id': 'roads_tunnels_medium',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['<', 'pmap:level', 0], ['==', 'pmap:kind', 'medium_road']],
        'paint': {
          'line-color': '#292929',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 7, 0, 12, 1.2, 15, 3, 18, 13],
        },
      },
      {
        'id': 'roads_tunnels_major',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['<', 'pmap:level', 0], ['==', 'pmap:kind', 'major_road']],
        'paint': {
          'line-color': '#292929',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 6, 0, 12, 1.6, 15, 3, 18, 13],
        },
      },
      {
        'id': 'roads_tunnels_highway',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['<', 'pmap:level', 0], ['==', 'pmap:kind', 'highway'], ['!=', 'pmap:link', 1]],
        'paint': {
          'line-color': '#292929',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 3, 0, 6, 1.1, 12, 1.6, 15, 5, 18, 15],
        },
      },
      {
        'id': 'buildings',
        'type': 'fill',
        'source': 'protomaps',
        'source-layer': 'buildings',
        'paint': {
          'fill-color': '#111',
          'fill-opacity': 0.5,
        },
      },
      {
        'id': 'transit_pier',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'transit',
        'filter': ['any', ['==', 'pmap:kind', 'pier']],
        'paint': {
          'line-color': '#333',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 12, 0, 12.5, 0.5, 20, 16],
        },
      },
      {
        'id': 'roads_minor_service_casing',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 14, // 13
        'filter': ['all', ['==', 'pmap:level', 0], ['==', 'pmap:kind', 'minor_road'], ['==', 'pmap:kind_detail', 'service']],
        'paint': {
          'line-color': '#1f1f1f',
          'line-gap-width': ['interpolate', ['exponential', 1.6], ['zoom'], 13, 0, 18, 8],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 13, 0, 13.5, 0.8],
        },
      },
      {
        'id': 'roads_minor_casing',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['==', 'pmap:level', 0], ['==', 'pmap:kind', 'minor_road'], ['!=', 'pmap:kind_detail', 'service']],
        'paint': {
          'line-color': '#1f1f1f',
          'line-gap-width': ['interpolate', ['exponential', 1.6], ['zoom'], 11, 0, 12.5, 0.5, 15, 2, 18, 11],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 12, 0, 12.5, 1],
        },
      },
      {
        'id': 'roads_link_casing',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 13,
        'filter': ['all', ['==', 'pmap:link', 1]],
        'paint': {
          'line-color': '#1f1f1f',
          'line-gap-width': ['interpolate', ['exponential', 1.6], ['zoom'], 13, 0, 13.5, 1, 18, 11],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 13, 0, 13.5, 1.5],
        },
      },
      {
        'id': 'roads_medium_casing',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 13, // added
        'filter': ['all', ['==', 'pmap:level', 0], ['==', 'pmap:kind', 'medium_road']],
        'paint': {
          'line-color': '#1f1f1f',
          'line-gap-width': ['interpolate', ['exponential', 1.6], ['zoom'], 7, 0, 12, 1.2, 15, 3, 18, 13],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 10, 0,  10.5, 1.5],
        },
      },
      {
        'id': 'roads_major_casing_late',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 12,
        'filter': ['all', ['==', 'pmap:level', 0], ['==', 'pmap:kind', 'major_road']],
        'paint': {
          'line-color': '#1f1f1f',
          'line-gap-width': ['interpolate', ['exponential', 1.6], ['zoom'], 6, 0, 12, 1.6, 15, 3, 18, 13],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 9, 0, 9.5, 1],
        },
      },
      {
        'id': 'roads_highway_casing_late',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 12,
        'filter': ['all', ['==', 'pmap:level', 0], ['==', 'pmap:kind', 'highway'], ['!=', 'pmap:link', 1]],
        'paint': {
          'line-color': '#1f1f1f',
          'line-gap-width': ['interpolate', ['exponential', 1.6], ['zoom'], 3, 0, 3.5, 0.5, 18, 15],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 7, 0, 7.5, 1, 20, 15],
        },
      },
      {
        'id': 'roads_other',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['==', 'pmap:level', 0], ['in', 'pmap:kind', 'other', 'path']],
        'paint': {
          'line-color': '#333',
          'line-dasharray': [3, 1],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 14, 0, 20, 7],
        },
      },
      {
        'id': 'roads_link',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['==', 'pmap:link', 1]],
        'paint': {
          'line-color': '#3d3d3d',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 13, 0, 13.5, 1, 18, 11],
        },
      },
      {
        'id': 'roads_minor_service',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['==', 'pmap:level', 0], ['==', 'pmap:kind', 'minor_road'], ['==', 'pmap:kind_detail', 'service']],
        'paint': {
          'line-color': '#333',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 13, 0, 18, 8],
        },
      },
      {
        'id': 'roads_minor',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['==', 'pmap:level', 0], ['==', 'pmap:kind', 'minor_road'], ['!=', 'pmap:kind_detail', 'service']],
        'paint': {
          'line-color': ['interpolate', ['exponential', 1.6], ['zoom'], 11, '#3d3d3d', 16, '#333'],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 11, 0, 12.5, 0.5, 15, 2, 18, 11],
        },
      },
      {
        'id': 'roads_medium',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 13, // added
        'filter': ['all', ['==', 'pmap:level', 0], ['==', 'pmap:kind', 'medium_road']],
        'paint': {
          'line-color': '#3d3d3d',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 7, 0, 12, 1.2, 15, 3, 18, 13],
        },
      },
      {
        'id': 'roads_major_casing_early',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'maxzoom': 12,
        'filter': ['all', ['==', 'pmap:level', 0], ['==', 'pmap:kind', 'major_road']],
        'paint': {
          'line-color': '#1f1f1f',
          'line-gap-width': ['interpolate', ['exponential', 1.6], ['zoom'], 7, 0, 7.5, 0.5, 18, 13],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 9, 0, 9.5, 1],
        },
      },
      {
        'id': 'roads_major',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['==', 'pmap:level', 0], ['==', 'pmap:kind', 'major_road']],
        'paint': {
          'line-color': '#3d3d3d',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 6, 0, 12, 1.6, 15, 3, 18, 13],
        },
      },
      {
        'id': 'roads_highway_casing_early',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'maxzoom': 12,
        'filter': ['all', ['==', 'pmap:level', 0], ['==', 'pmap:kind', 'highway'], ['!=', 'pmap:link', 1]],
        'paint': {
          'line-color': '#1f1f1f',
          'line-gap-width': ['interpolate', ['exponential', 1.6], ['zoom'], 3, 0, 3.5, 0.5, 18, 15],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 7, 0, 7.5, 1],
        },
      },
      {
        'id': 'roads_highway',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['==', 'pmap:level', 0], ['==', 'pmap:kind', 'highway'], ['!=', 'pmap:link', 1]],
        'paint': {
          'line-color': '#474747',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 3, 0, 6, 1.1, 12, 1.6, 15, 5, 18, 15],
        },
      },
      {
        'id': 'transit_railway',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'transit',
        'filter': ['all', ['==', 'pmap:kind', 'rail']],
        'paint': {
          'line-dasharray': [0.3, 0.75],
          'line-opacity': 0.5,
          'line-color': '#000',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 3, 0, 6, 0.15, 18, 9],
        },
      },
      {
        'id': 'boundaries_country',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'boundaries',
        'filter': ['<=', 'pmap:min_admin_level', 2],
        'paint': {
          'line-color': '#5b6374',
          'line-width': 1,
          'line-dasharray': [3,  2],
        },
      },
      {
        'id': 'boundaries',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'boundaries',
        'filter': ['>', 'pmap:min_admin_level', 2],
        'paint': {
          'line-color': '#5b6374',
          'line-width': 0.5,
          'line-dasharray': [3,  2],
        },
      },
      {
        'id': 'roads_bridges_other_casing',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 12,
        'filter': ['all', ['>', 'pmap:level', 0],
          ['in', 'pmap:kind', 'other', 'path']],
        'paint': {
          'line-color': '#2b2b2b',
          'line-gap-width': ['interpolate', ['exponential', 1.6], ['zoom'], 14, 0, 20, 7],
        },
      },
      {
        'id': 'roads_bridges_link_casing',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 12,
        'filter': ['all', ['>', 'pmap:level', 0], ['==', 'pmap:link', 1]],
        'paint': {
          'line-color': '#1f1f1f',
          'line-gap-width': ['interpolate', ['exponential', 1.6], ['zoom'], 13, 0, 13.5, 1, 18, 11],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 12, 0, 12.5, 1.5],
        },
      },
      {
        'id': 'roads_bridges_minor_casing',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 12,
        'filter': ['all', ['>', 'pmap:level', 0], ['==', 'pmap:kind', 'minor_road']],
        'paint': {
          'line-color': '#1f1f1f',
          'line-gap-width': ['interpolate', ['exponential', 1.6], ['zoom'], 11, 0, 12.5, 0.5, 15, 2, 18, 11],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 13, 0, 13.5, 0.8],
        },
      },
      {
        'id': 'roads_bridges_medium_casing',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 12,
        'filter': ['all', ['>', 'pmap:level', 0], ['==', 'pmap:kind', 'medium_road']],
        'paint': {
          'line-color': '#1f1f1f',
          'line-gap-width': ['interpolate', ['exponential', 1.6], ['zoom'], 7, 0, 12, 1.2, 15, 3, 18, 13],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 10, 0,  10.5, 1.5],
        },
      },
      {
        'id': 'roads_bridges_major_casing',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 12,
        'filter': ['all', ['>', 'pmap:level', 0], ['==', 'pmap:kind', 'major_road']],
        'paint': {
          'line-color': '#1f1f1f',
          'line-gap-width': ['interpolate', ['exponential', 1.6], ['zoom'], 7, 0, 7.5, 0.5, 18, 10],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 9, 0, 9.5, 1.5],
        },
      },
      {
        'id': 'roads_bridges_other',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 12,
        'filter': ['all', ['>', 'pmap:level', 0], ['in', 'pmap:kind', 'other', 'path']],
        'paint': {
          'line-color': '#333',
          'line-dasharray': [2, 1],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 14, 0, 20, 7],
        },
      },
      {
        'id': 'roads_bridges_minor',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 12,
        'filter': ['all', ['>', 'pmap:level', 0], ['==', 'pmap:kind', 'minor_road']],
        'paint': {
          'line-color': '#333',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 11, 0, 12.5, 0.5, 15, 2, 18, 11],
        },
      },
      {
        'id': 'roads_bridges_link',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 12,
        'filter': ['all', ['>', 'pmap:level', 0], ['==', 'pmap:link', 1]],
        'paint': {
          'line-color': '#333',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 13, 0, 13.5, 1, 18, 11],
        },
      },
      {
        'id': 'roads_bridges_medium',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 12,
        'filter': ['all', ['>', 'pmap:level', 0], ['==', 'pmap:kind', 'medium_road']],
        'paint': {
          'line-color': '#3d3d3d',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 7, 0, 12, 1.2, 15, 3, 18, 13],
        },
      },
      {
        'id': 'roads_bridges_major',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 12,
        'filter': ['all', ['>', 'pmap:level', 0], ['==', 'pmap:kind', 'major_road']],
        'paint': {
          'line-color': '#3d3d3d',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 6, 0, 12, 1.6, 15, 3, 18, 13],
        },
      },
      {
        'id': 'roads_bridges_highway_casing',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 12,
        'filter': ['all', ['>', 'pmap:level', 0], ['==', 'pmap:kind', 'highway'], ['!=', 'pmap:link', 1]],
        'paint': {
          'line-color': '#1f1f1f',
          'line-gap-width': ['interpolate', ['exponential', 1.6], ['zoom'], 3, 0, 3.5, 0.5, 18, 15],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 7, 0, 7.5, 1, 20, 15],
        },
      },
      {
        'id': 'roads_bridges_highway',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['>', 'pmap:level', 0], ['==', 'pmap:kind', 'highway'], ['!=', 'pmap:link', 1]],
        'paint': {
          'line-color': '#474747',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 3, 0, 6, 1.1, 12, 1.6, 15, 5, 18, 15],
        },
      },
      {
        'id': 'physical_line_waterway_label',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'physical_line',
        'minzoom': 13,
        'filter': ['all', ['in', 'pmap:kind', 'river', 'stream']],
        'layout': {
          'symbol-placement': 'line',
          'text-font': ['Noto Sans Regular'],
          'text-field': ['get', 'name'],
          'text-size': 12,
          'text-letter-spacing': 0.3,
        },
        'paint': {
          'text-color': '#717784',
        },
      },
      {
        'id': 'physical_point_peak',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'physical_point',
        'filter': ['any', ['==', 'pmap:kind', 'peak']],
        'layout': {
          'text-font': ['Noto Sans Italic'],
          'text-field': ['get', 'name'],
          'text-size': ['interpolate', ['linear'], ['zoom'], 10, 8, 16, 12],
          'text-letter-spacing': 0.1,
          'text-max-width': 9,
        },
        'paint': {
          'text-color': '#898080',
          'text-halo-width': 1.5,
        },
      },
      {
        'id': 'roads_labels_minor',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 16, // 15
        'filter': ['any', ['in', 'pmap:kind', 'minor_road', 'other', 'path']],
        'layout': {
          'symbol-sort-key': ['get', 'pmap:min_zoom'],
          'symbol-placement': 'line',
          'text-font': ['Noto Sans Regular'],
          'text-field': ['get', 'name'],
          'text-size': 12,
        },
        'paint': {
          'text-color': '#525252',
          'text-halo-color': '#1f1f1f',
          'text-halo-width': 2,
        },
      },
      {
        'id': 'physical_point_ocean',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'physical_point',
        'filter': ['any', ['in', 'pmap:kind', 'sea', 'ocean', 'lake', 'water', 'bay', 'strait', 'fjord']],
        'layout': {
          'text-font': ['Noto Sans Medium'],
          'text-field': ['get', 'name'],
          'text-size': ['interpolate', ['linear'], ['zoom'], 3, 10, 10, 12],
          'text-letter-spacing': 0.1,
          'text-max-width': 9,
          'text-transform': 'uppercase',
        },
        'paint': {
          'text-color': '#717784',
        },
      },
      {
        'id': 'physical_point_lakes',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'physical_point',
        'filter': ['any', ['in', 'pmap:kind', 'lake', 'water']],
        'layout': {
          'text-font': ['Noto Sans Medium'],
          'text-field': ['get', 'name'],
          'text-size': ['interpolate', ['linear'], ['zoom'], 3, 0, 6, 12, 10, 12],
          'text-letter-spacing': 0.1,
          'text-max-width': 9,
        },
        'paint': {
          'text-color': '#717784',
        },
      },
      {
        'id': 'roads_labels_major',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 13, // 11
        'filter': ['any', ['in', 'pmap:kind', 'highway', 'major_road', 'medium_road']],
        'layout': {
          'symbol-sort-key': ['get', 'pmap:min_zoom'],
          'symbol-placement': 'line',
          'text-font': ['Noto Sans Regular'],
          'text-field': ['get', 'name'],
          'text-size': 12,
        },
        'paint': {
          'text-color': '#666',
          'text-halo-color': '#1f1f1f',
          'text-halo-width': 2,
        },
      },
      {
        'id': 'places_subplace',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'places',
        'filter': ['==', 'pmap:kind', 'neighbourhood'],
        'layout': {
          'symbol-sort-key': ['get', 'pmap:min_zoom'],
          'text-field': '{name}',
          'text-font': ['Noto Sans Regular'],
          'text-max-width': 7,
          'text-letter-spacing': 0.1,
          'text-padding': ['interpolate', ['linear'], ['zoom'], 5, 2, 8, 4, 12, 18, 15, 20],
          'text-size': ['interpolate', ['exponential', 1.2], ['zoom'], 11, 8, 14, 14, 18, 24],
          'text-transform': 'uppercase',
        },
        'paint': {
          'text-color': '#525252',
          'text-halo-color': '#1f1f1f',
          'text-halo-width': 2,
        },
      },
      {
        'id': 'pois_important',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'pois',
        'minzoom': 13, // added
        'filter': ['any', ['<', ['get', 'pmap:min_zoom'], 15]],
        'layout': {
          'symbol-sort-key': ['get', 'pmap:min_zoom'],
          'text-font': ['Noto Sans Regular'],
          'text-field': ['get', 'name'],
          'text-size': 11,
          'text-max-width': 9,
          'icon-padding': ['interpolate', ['linear'], ['zoom'], 0, 2, 14, 2, 16, 20, 17, 2, 22, 2],
        },
        'paint': {
          'text-color': '#525252',
          'text-halo-color': '#1f1f1f',
          'text-halo-width': 1.5,
        },
      },

      ...metroLayers,

      // Based on https://maplibre.org/maplibre-gl-js/docs/examples/3d-buildings/
      // and https://svelte-maplibre.vercel.app/examples/3d_buildings
      {
        'id': '3d-buildings',
        'type': 'fill-extrusion',
        'source': 'protomaps',
        'source-layer': 'buildings',
        'minzoom': 14,
        'paint': {
          'fill-extrusion-color': '#080808',
          'fill-extrusion-height': ['interpolate', ['linear'], ['zoom'], 14, 0, 14.05, ['get', 'height']],
          'fill-extrusion-base': ['interpolate', ['linear'], ['zoom'], 14, 0, 14.05, 0],
          'fill-extrusion-opacity': 0.4
        }
      },
      {
        'id': 'places_locality_circle',
        'type': 'circle',
        'source': 'protomaps',
        'source-layer': 'places',
        'filter': ['==', 'pmap:kind',  'locality'],
        'paint': {
          'circle-radius': 2,
          'circle-stroke-width': 1.5,
          'circle-stroke-color': '#7a7a7a',
          'circle-color': '#000',
          'circle-translate': [-6, 0],
        },
        'maxzoom': 8,
      },
      {
        'id': 'places_locality',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'places',
        'filter': ['==', 'pmap:kind',  'locality'],
        'layout': {
          'text-field': '{name}',
          'text-font': ['case', ['<=', ['get', 'pmap:min_zoom'], 5], ['literal', ['Noto Sans Medium']], ['literal', ['Noto Sans Regular']]],
          'text-padding': ['interpolate', ['linear'], ['zoom'], 5, 3, 8, 7, 12, 11],
          'text-size': [
            'interpolate',
            ['linear'],
            ['zoom'],
            2,
            ['case', ['<', ['get', 'pmap:population_rank'], 13], 8, ['>=', ['get', 'pmap:population_rank'], 13], 13, 0],
            4,
            ['case', ['<', ['get', 'pmap:population_rank'], 13], 10, ['>=', ['get', 'pmap:population_rank'], 13], 15, 0],
            6,
            ['case', ['<', ['get', 'pmap:population_rank'], 12], 11, ['>=', ['get', 'pmap:population_rank'], 12], 17, 0],
            8,
            ['case', ['<', ['get', 'pmap:population_rank'], 11], 11, ['>=', ['get', 'pmap:population_rank'], 11], 18, 0],
            10,
            ['case', ['<', ['get', 'pmap:population_rank'], 9], 12, ['>=', ['get', 'pmap:population_rank'], 9], 20, 0],
            15,
            ['case', ['<', ['get', 'pmap:population_rank'], 8], 12, ['>=', ['get', 'pmap:population_rank'], 8], 22, 0],
          ],
          'icon-padding': ['interpolate', ['linear'], ['zoom'], 0, 2, 8, 4, 10, 8, 12, 6, 22, 2],
          'text-anchor': ['step', ['zoom'], 'left', 8, 'center'],
          'text-radial-offset': 0.2,
        },
        'paint': {
          'text-color': '#7a7a7a',
          'text-halo-color': '#212121',
          'text-halo-width': 1,
        },
      },
      {
        'id': 'places_region',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'places',
        'filter': ['==', 'pmap:kind', 'region'],
        'layout': {
          'symbol-sort-key': ['get', 'pmap:min_zoom'],
          'text-field': ['step', ['zoom'], ['get', 'name:short'], 6, ['get', 'name']],
          'text-font': ['Noto Sans Regular'],
          'text-size': ['interpolate', ['linear'], ['zoom'], 3, 11, 7, 16],
          'text-radial-offset': 0.2,
          'text-anchor': 'center',
          'text-transform': 'uppercase',
        },
        'paint': {
          'text-color': '#3d3d3d',
          'text-halo-color': '#1f1f1f',
          'text-halo-width': 2,
        },
      },
      {
        'id': 'places_country',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'places',
        'filter': ['==', 'pmap:kind', 'country'],
        'layout': {
          'symbol-sort-key': ['get', 'pmap:min_zoom'],
          'text-field': '{name}',
          'text-font': ['Noto Sans Medium'],
          'text-size': [
            'interpolate',
            ['linear'],
            ['zoom'],
            2,
            ['case', ['<', ['get', 'pmap:population_rank'], 10], 8, ['>=', ['get', 'pmap:population_rank'], 10], 12, 0],
            6,
            ['case', ['<', ['get', 'pmap:population_rank'], 8], 10, ['>=', ['get', 'pmap:population_rank'], 8], 18, 0],
            8,
            ['case', ['<', ['get', 'pmap:population_rank'], 7], 11, ['>=',  ['get', 'pmap:population_rank'], 7],20, 0],
          ],
          'icon-padding': ['interpolate', ['linear'], ['zoom'], 0, 2, 14, 2, 16, 20, 17, 2, 22, 2],
          'text-transform': 'uppercase',
        },
        'paint': {
          'text-color': '#5c5c5c',
        },
      },
    ],
  },

  hybrid: {
    version: 8,
    id: 'orto',
    name: 'Hybrid',
    sources: {
      'ortoEsri': {
        type: 'raster',
        tiles: [
          'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
        ],
        tileSize: 256,
        maxzoom: 18,
        attribution: '<a href=\'http://www.esri.com\'>&copy; Esri</a>',
      },
      'protomaps': {
        type: 'vector',
        url: 'pmtiles://' + pmtilesUrl,
        attribution: '<a href="https://protomaps.com/">&copy; Protomaps</a> | <a href="https://openstreetmap.org">&copy; OpenStreetMap</a>',
      },
      ...metroSource,
    },

    glyphs: 'https://protomaps.github.io/basemaps-assets/fonts/{fontstack}/{range}.pbf',
    layers: [
      {
        'id': 'ortoEsri',
        'type': 'raster',
        'source': 'ortoEsri',
        //'maxzoom': 16,
        'layout': {
          'visibility': 'visible',
        },
      },
      {
        'id': 'roads_tunnels_other',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['<', 'pmap:level', 0], ['in', 'pmap:kind', 'other', 'path']],
        'paint': {
          'line-color': 'hsla(0, 0%, 100%, 0.2)',
          'line-width': ['interpolate', ['exponential', 1.5], ['zoom'], 14, 0.5, 20, 4],
          'line-dasharray': [1, 1],
        },
      },
      {
        'id': 'roads_tunnels_minor',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['<', 'pmap:level', 0], ['==', 'pmap:kind', 'minor_road']],
        'paint': {
          'line-color': 'hsla(0, 0%, 100%, 0.2)',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 12, 0, 12.5, 1],
          'line-dasharray': [0.5, 0.25],
        },
      },
      {
        'id': 'roads_tunnels_medium',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['<', 'pmap:level', 0], ['==', 'pmap:kind', 'medium_road']],
        'paint': {
          'line-color': 'hsla(0, 0%, 100%, 0.2)',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 14, 0, 20, 7],
          'line-dasharray': [0.5, 0.25],
        },
      },
      {
        'id': 'roads_tunnels_major',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['<', 'pmap:level', 0], ['==', 'pmap:kind', 'major_road']],
        'paint': {
          'line-color': 'hsla(0, 0%, 100%, 0.2)',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 14, 0, 20, 7],
          'line-dasharray': [0.5, 0.25],
        },
      },
      {
        'id': 'roads_tunnels_highway',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['<', 'pmap:level', 0], ['==', 'pmap:kind', 'highway'], ['!=', 'pmap:link', 1]],
        'paint': {
          'line-color': 'hsla(0, 0%, 100%, 0.2)',
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 14, 0, 20, 7],
          'line-dasharray': [0.5, 0.25],
        },
      },
      {
        'id': 'transit_pier',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'transit',
        'filter': ['any', ['==', 'pmap:kind', 'pier']],
        'paint': {
          'line-color': 'hsla(0, 0%, 97%, 0.33)',
          'line-width': ['interpolate', ['exponential', 1.5], ['zoom'], 14, 0.5, 20, 4],
          'line-dasharray': [1, 1],
        },
      },
      {
        'id': 'roads_other',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['==', 'pmap:level', 0], ['in', 'pmap:kind', 'other', 'path']],
        'paint': {
          'line-color': 'hsla(0, 0%, 97%, 0.33)',
          'line-width': ['interpolate', ['exponential', 1.5], ['zoom'], 14, 0.5, 20, 4],
          'line-dasharray': [1, 1],
        },
      },
      {
        'id': 'roads_minor_service',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['==', 'pmap:level', 0], ['==', 'pmap:kind', 'minor_road'], ['==', 'pmap:kind_detail', 'service']],
        'paint': {
          'line-color': {'stops': [[8, 'hsla(0, 0%, 100%, 0.2)'], [14, 'hsla(0, 0%, 100%, 0.4)'], [18, 'hsla(0, 0%, 100%, 0.5)']]},
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 12, 0, 12.5, 1],
        },
      },
      {
        'id': 'roads_minor',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['==', 'pmap:level', 0], ['==', 'pmap:kind', 'minor_road'], ['!=', 'pmap:kind_detail', 'service']],
        'paint': {
          'line-color': {'stops': [[8, 'hsla(0, 0%, 100%, 0.2)'], [14, 'hsla(0, 0%, 100%, 0.4)'], [18, 'hsla(0, 0%, 100%, 0.5)']]},
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 12, 0, 12.5, 1],
        },
      },
      {
        'id': 'roads_medium',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 13, // added
        'filter': ['all', ['==', 'pmap:level', 0], ['==', 'pmap:kind', 'medium_road']],
        'paint': {
          'line-color': {'stops': [[8, 'hsla(0, 0%, 100%, 0.2)'], [14, 'hsla(0, 0%, 100%, 0.4)'], [18, 'hsla(0, 0%, 100%, 0.5)']]},
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 14, 0, 20, 7],
        },
      },
      {
        'id': 'roads_major',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['==', 'pmap:level', 0], ['==', 'pmap:kind', 'major_road']],
        'paint': {
          'line-color': {'stops': [[8, 'hsla(0, 0%, 100%, 0.2)'], [14, 'hsla(0, 0%, 100%, 0.4)'], [18, 'hsla(0, 0%, 100%, 0.5)']]},
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 14, 0, 20, 7],
        },
      },
      {
        'id': 'roads_highway',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['==', 'pmap:level', 0], ['==', 'pmap:kind', 'highway'], ['!=', 'pmap:link', 1]],
        'paint': {
          'line-color': {'stops': [[8, 'hsla(0, 0%, 100%, 0.2)'], [14, 'hsla(0, 0%, 100%, 0.4)'], [18, 'hsla(0, 0%, 100%, 0.5)']]},
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 14, 0, 20, 7],
        },
      },
      {
        'id': 'transit_railway',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'transit',
        'filter': ['all', ['==', 'pmap:kind', 'rail']],
        'paint': {
          'line-color': {'stops': [[8, 'hsla(0, 0%, 100%, 0.2)'], [14, 'hsla(0, 0%, 100%, 0.4)'], [18, 'hsla(0, 0%, 100%, 0.5)']]},
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 12, 0, 12.5, 1],
        },
      },
      {
        'id': 'boundaries_country',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'boundaries',
        'filter': ['<=', 'pmap:min_admin_level', 2],
        'paint': {
          'line-color': '#5b6374',
          'line-width': 1,
          'line-dasharray': [3, 2],
        },
      },
      {
        'id': 'boundaries',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'boundaries',
        'filter': ['>', 'pmap:min_admin_level', 2],
        'paint': {
          'line-color': '#5b6374',
          'line-width': 0.5,
          'line-dasharray': [3, 2],
        },
      },
      {
        'id': 'roads_bridges_other',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 12,
        'filter': ['all', ['>', 'pmap:level', 0], ['in', 'pmap:kind', 'other', 'path']],
        'paint': {
          'line-color': {'stops': [[8, 'hsla(0, 0%, 100%, 0.2)'], [14, 'hsla(0, 0%, 100%, 0.4)'], [18, 'hsla(0, 0%, 100%, 0.5)']]},
          'line-dasharray': [1, 1],
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 12, 0, 12.5, 1],
        },
      },
      {
        'id': 'roads_bridges_minor',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 12,
        'filter': ['all', ['>', 'pmap:level', 0], ['==', 'pmap:kind', 'minor_road']],
        'paint': {
          'line-color': {'stops': [[8, 'hsla(0, 0%, 100%, 0.2)'], [14, 'hsla(0, 0%, 100%, 0.4)'], [18, 'hsla(0, 0%, 100%, 0.5)']]},
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 12, 0, 12.5, 1],
        },
      },
      {
        'id': 'roads_bridges_medium',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 12,
        'filter': ['all', ['>', 'pmap:level', 0], ['==', 'pmap:kind', 'medium_road']],
        'paint': {
          'line-color': {'stops': [[8, 'hsla(0, 0%, 100%, 0.2)'], [14, 'hsla(0, 0%, 100%, 0.4)'], [18, 'hsla(0, 0%, 100%, 0.5)']]},
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 14, 0, 20, 7],
        },
      },
      {
        'id': 'roads_bridges_major',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 12,
        'filter': ['all', ['>', 'pmap:level', 0], ['==', 'pmap:kind', 'major_road']],
        'paint': {
          'line-color': {'stops': [[8, 'hsla(0, 0%, 100%, 0.2)'], [14, 'hsla(0, 0%, 100%, 0.4)'], [18, 'hsla(0, 0%, 100%, 0.5)']]},
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 14, 0, 20, 7],
        },
      },
      {
        'id': 'roads_bridges_highway',
        'type': 'line',
        'source': 'protomaps',
        'source-layer': 'roads',
        'filter': ['all', ['>', 'pmap:level', 0], ['==', 'pmap:kind', 'highway'], ['!=', 'pmap:link', 1]],
        'paint': {
          'line-color': {'stops': [[8, 'hsla(0, 0%, 100%, 0.2)'], [14, 'hsla(0, 0%, 100%, 0.4)'], [18, 'hsla(0, 0%, 100%, 0.5)']]},
          'line-width': ['interpolate', ['exponential', 1.6], ['zoom'], 14, 0, 20, 7],
        },
      },
      {
        'id': 'physical_line_waterway_label',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'physical_line',
        'minzoom': 13,
        'filter': ['all', ['in', 'pmap:kind', 'river', 'stream']],
        'layout': {
          'symbol-placement': 'line',
          'text-font': ['Noto Sans Regular'],
          'text-field': ['get', 'name'],
          'text-size': 12,
          'text-letter-spacing': 0.3,
        },
        'paint': {
          'text-color': '#717784',
        },
      },
      {
        'id': 'physical_point_peak',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'physical_point',
        'filter': ['any', ['==', 'pmap:kind', 'peak']],
        'layout': {
          'text-font': ['Noto Sans Italic'],
          'text-field': ['get', 'name'],
          'text-size': ['interpolate', ['linear'], ['zoom'], 10, 8, 16, 12],
          'text-letter-spacing': 0.1,
          'text-max-width': 9,
        },
        'paint': {
          'text-color': '#898080',
          'text-halo-width': 1.5,
        },
      },
      {
        'id': 'roads_labels_minor',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 16, // 15
        'filter': ['any', ['in', 'pmap:kind', 'minor_road', 'other', 'path']],
        'layout': {
          'symbol-sort-key': ['get', 'pmap:min_zoom'],
          'symbol-placement': 'line',
          'text-font': ['Noto Sans Regular'],
          'text-field': ['get', 'name'],
          'text-size': 12,
        },
        'paint': {
          'text-color': 'hsl(0, 0%, 100%)',
          'text-halo-color': 'hsl(0, 0%, 17%)',
          'text-halo-width': 1,
        },
      },
      {
        'id': 'physical_point_ocean',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'physical_point',
        'filter': ['any', ['in', 'pmap:kind', 'sea', 'ocean', 'lake', 'water', 'bay', 'strait', 'fjord']],
        'layout': {
          'text-font': ['Noto Sans Medium'],
          'text-field': ['get', 'name'],
          'text-size': ['interpolate', ['linear'], ['zoom'], 3, 10, 10, 12],
          'text-letter-spacing': 0.1,
          'text-max-width': 9,
          'text-transform': 'uppercase',
        },
        'paint': {
          'text-color': 'hsl(0, 0%, 100%)',
        },
      },
      {
        'id': 'physical_point_lakes',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'physical_point',
        'filter': ['any', ['in', 'pmap:kind', 'lake', 'water']],
        'layout': {
          'text-font': ['Noto Sans Medium'],
          'text-field': ['get', 'name'],
          'text-size': ['interpolate', ['linear'], ['zoom'], 3, 0, 6, 12, 10, 12],
          'text-letter-spacing': 0.1,
          'text-max-width': 9,
        },
        'paint': {
          'text-color': 'hsl(0, 0%, 100%)',
        },
      },
      {
        'id': 'roads_labels_major',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'roads',
        'minzoom': 13, // 11
        'filter': ['any', ['in', 'pmap:kind', 'highway', 'major_road', 'medium_road']],
        'layout': {
          'symbol-sort-key': ['get', 'pmap:min_zoom'],
          'symbol-placement': 'line',
          'text-font': ['Noto Sans Regular'],
          'text-field': ['get', 'name'],
          'text-size': 12,
        },
        'paint': {
          'text-color': 'hsl(0, 0%, 100%)',
          'text-halo-color': 'hsl(0, 0%, 17%)',
          'text-halo-width': 1,
        },
      },
      {
        'id': 'places_subplace',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'places',
        'filter': ['==', 'pmap:kind', 'neighbourhood'],
        'layout': {
          'symbol-sort-key': ['get', 'pmap:min_zoom'],
          'text-field': '{name}',
          'text-font': ['Noto Sans Regular'],
          'text-max-width': 7,
          'text-letter-spacing': 0.1,
          'text-padding': ['interpolate', ['linear'], ['zoom'], 5, 2, 8, 4, 12, 18, 15, 20],
          'text-size': ['interpolate', ['exponential', 1.2], ['zoom'], 11, 8, 14, 14, 18, 24],
          'text-transform': 'uppercase',
        },
        'paint': {
          'text-color': 'hsl(0, 0%, 100%)',
          'text-halo-blur': 0.5,
          'text-halo-color': 'hsl(0, 0%, 0%)',
          'text-halo-width': 1,
        },
      },
      {
        'id': 'pois_important',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'pois',
        'minzoom': 13, // added
        'filter': ['any', ['<', ['get', 'pmap:min_zoom'], 15]],
        'layout': {
          'symbol-sort-key': ['get', 'pmap:min_zoom'],
          'text-font': ['Noto Sans Regular'],
          'text-field': ['get', 'name'],
          'text-size': 11,
          'text-max-width': 9,
          'icon-padding': ['interpolate', ['linear'], ['zoom'], 0, 2, 14, 2, 16, 20, 17, 2, 22, 2],
        },
        'paint': {
          'text-color': 'hsl(0, 0%, 100%)',
          'text-halo-blur': 0.5,
          'text-halo-color': 'hsl(0, 0%, 0%)',
          'text-halo-width': 1,
        },
      },

      ...metroLayers,

      {
        'id': 'places_locality_circle',
        'type': 'circle',
        'source': 'protomaps',
        'source-layer': 'places',
        'filter': ['==', 'pmap:kind',  'locality'],
        'paint': {
          'circle-radius': 2,
          'circle-stroke-width': 1.5,
          'circle-stroke-color': '#7a7a7a',
          'circle-color': '#000',
          'circle-translate': [-6, 0],
        },
        'maxzoom': 8,
      },
      {
        'id': 'places_locality',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'places',
        'filter': ['==', 'pmap:kind', 'locality'],
        'layout': {
          'text-field': '{name}',
          'text-font': ['case', ['<=', ['get', 'pmap:min_zoom'], 5], ['literal', ['Noto Sans Medium']], ['literal', ['Noto Sans Regular']]],
          'text-padding': ['interpolate', ['linear'], ['zoom'], 5, 3, 8, 7, 12, 11],
          'text-size': [
            'interpolate',
            ['linear'],
            ['zoom'],
            2,
            ['case', ['<', ['get', 'pmap:population_rank'], 13], 8, ['>=', ['get', 'pmap:population_rank'], 13], 13, 0],
            4,
            ['case', ['<', ['get', 'pmap:population_rank'], 13], 10, ['>=', ['get', 'pmap:population_rank'], 13], 15, 0],
            6,
            ['case', ['<', ['get', 'pmap:population_rank'], 12], 11, ['>=', ['get', 'pmap:population_rank'], 12], 17, 0],
            8,
            ['case', ['<', ['get', 'pmap:population_rank'], 11], 11, ['>=', ['get', 'pmap:population_rank'], 11], 18, 0],
            10,
            ['case', ['<', ['get', 'pmap:population_rank'], 9], 12, ['>=', ['get', 'pmap:population_rank'], 9], 20, 0],
            15,
            ['case', ['<', ['get', 'pmap:population_rank'], 8], 12, ['>=', ['get', 'pmap:population_rank'], 8], 22, 0],
          ],
          'icon-padding': ['interpolate', ['linear'], ['zoom'], 0, 2, 8, 4, 10, 8, 12, 6, 22, 2],
          'text-anchor': ['step', ['zoom'], 'left', 8, 'center'],
          'text-radial-offset': 0.2,
        },
        'paint': {
          'text-color': 'hsl(0, 0%, 100%)',
          'text-halo-blur': 0.5,
          'text-halo-color': 'hsl(0, 0%, 0%)',
          'text-halo-width': 1,
        },
      },
      {
        'id': 'places_region',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'places',
        'filter': ['==', 'pmap:kind', 'region'],
        'layout': {
          'symbol-sort-key': ['get', 'pmap:min_zoom'],
          'text-field': ['step', ['zoom'], ['get', 'name:short'], 6, ['get', 'name']],
          'text-font': ['Noto Sans Regular'],
          'text-size': ['interpolate', ['linear'], ['zoom'], 3, 11, 7, 16],
          'text-radial-offset': 0.2,
          'text-anchor': 'center',
          'text-transform': 'uppercase',
        },
        'paint': {
          'text-color': 'hsl(0, 0%, 100%)',
          'text-halo-blur': 1,
          'text-halo-color': 'hsl(0, 0%, 0%)',
          'text-halo-width': 1,
        },
      },
      {
        'id': 'places_country',
        'type': 'symbol',
        'source': 'protomaps',
        'source-layer': 'places',
        'filter': ['==', 'pmap:kind', 'country'],
        'layout': {
          'symbol-sort-key': ['get', 'pmap:min_zoom'],
          'text-field': '{name}',
          'text-font': ['Noto Sans Medium'],
          'text-size': [
            'interpolate',
            ['linear'],
            ['zoom'],
            2,
            ['case', ['<', ['get', 'pmap:population_rank'], 10], 8, ['>=', ['get', 'pmap:population_rank'], 10], 12, 0],
            6,
            ['case', ['<', ['get', 'pmap:population_rank'], 8], 10, ['>=', ['get', 'pmap:population_rank'], 8], 18, 0],
            8,
            ['case', ['<', ['get', 'pmap:population_rank'], 7], 11, ['>=',  ['get', 'pmap:population_rank'], 7], 20, 0],
          ],
          'icon-padding': ['interpolate', ['linear'], ['zoom'], 0, 2, 14, 2, 16, 20, 17, 2, 22, 2],
          'text-transform': 'uppercase',
        },
        'paint': {
          'text-color': 'hsl(0, 0%, 100%)',
          'text-halo-blur': 1,
          'text-halo-color': 'hsl(0, 0%, 0%)',
          'text-halo-width': 1,
        },
      },
    ],
  },
})

// Map style switcher.
// Inspired by https://github.com/astridx/maplibreexamples/blob/main/plugins/maplibre-style-switcher.html
class MapLibreStyleSwitcherControl {
  constructor(styles, defaultStyle) {
    this.styles = styles || MapLibreStyleSwitcherControl.DEFAULT_STYLES;
    this.defaultStyle =
      defaultStyle || MapLibreStyleSwitcherControl.DEFAULT_STYLE;
    this.onDocumentClick = this.onDocumentClick.bind(this);
  };

  getDefaultPosition() {
    return 'top-right';
  };

  onAdd(map) {
    this.map = map;
    this.controlContainer = document.createElement('div');
    this.controlContainer.classList.add('maplibregl-ctrl');
    this.controlContainer.classList.add('maplibregl-ctrl-group');
    this.mapStyleContainer = document.createElement('div');
    this.styleButton = document.createElement('button');
    this.styleButton.type = 'button';
    this.mapStyleContainer.classList.add('maplibregl-style-list');
    for (const style in this.styles) {
      const styleElement = document.createElement('button');
      styleElement.type = 'button';
      styleElement.innerText = this.styles[style].name;
      styleElement.classList.add(
        this.styles[style].name.replace(/[^a-z0-9-]/gi, '_'),
      );

      styleElement.addEventListener('click', (event) => {
        const srcElement = event.target;
        if (srcElement.classList.contains('active')) {
          return;
        }
        this.map.setStyle(this.styles[style]);
        localStorage.setItem('map-style', style);
        this.mapStyleContainer.style.display = 'none';
        this.styleButton.style.display = 'block';
        const elms =
          this.mapStyleContainer.getElementsByClassName('active');
        while (elms[0]) {
          elms[0].classList.remove('active');
        }
        srcElement.classList.add('active');
      });
      if (this.styles[style].name === this.defaultStyle.name) {
        styleElement.classList.add('active');
      }
      this.mapStyleContainer.appendChild(styleElement);
    }

    this.styleButton.classList.add('maplibregl-ctrl-icon');
    this.styleButton.classList.add('maplibregl-style-switcher');
    this.styleButton.addEventListener('click', () => {
      this.styleButton.style.display = 'none';
      this.mapStyleContainer.style.display = 'block';
    });
    document.addEventListener('click', this.onDocumentClick);
    this.controlContainer.appendChild(this.styleButton);
    this.controlContainer.appendChild(this.mapStyleContainer);
    return this.controlContainer;
  };

  onRemove() {
    if (
      !this.controlContainer ||
      !this.controlContainer.parentNode ||
      !this.map ||
      !this.styleButton
    ) {
      return;
    }
    this.styleButton.removeEventListener('click', this.onDocumentClick);
    this.controlContainer.parentNode.removeChild(this.controlContainer);
    document.removeEventListener('click', this.onDocumentClick);
    this.map = undefined;
  };

  onDocumentClick(event) {
    if (
      this.controlContainer &&
      !this.controlContainer.contains(event.target) &&
      this.mapStyleContainer &&
      this.styleButton
    ) {
      this.mapStyleContainer.style.display = 'none';
      this.styleButton.style.display = 'block';
    }
  };
}

// Infowindow handler.
const showDescriptionPopup = (e, map) => {
  if (typeof e.features[0].properties.description === 'undefined') {
    return;
  }

  const description = JSON.parse(e.features[0].properties.description);
  new maplibregl.Popup()
    .setLngLat(e.lngLat)
    .setHTML(description.value)
    .addTo(map);
};
