<?php

namespace Drupal\geolocation_jsvalues_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'geolocation_jsvalues' formatter.
 *
 * @FieldFormatter(
 *   id = "geolocation_jsvalues",
 *   module = "geolocation_jsvalues_formatter",
 *   label = @Translation("Geolocation JS values"),
 *   field_types = {
 *     "geolocation"
 *   }
 * )
 */
class GeolocationJSValuesFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();
    //$lat = '';
    //$lng = '';
    $lat = 0;
    $lng = 0;
    foreach ($items as $delta => $item) {
      if ($item->lat) { $lat = (float) $item->lat; }
      if ($item->lng) { $lng = (float) $item->lng; }
      $elements[$delta] = array(
        '#attached' => [
          'drupalSettings' => [
            'geolocationJSValuesFormatter' => [
              'lat' => $lat,
              'lng' => $lng,
            ],
          ],
        ],
      );
    }

    return $elements;
  }

}
