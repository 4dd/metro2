<?php

namespace Drupal\file_svg_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;

/**
 * Plugin implementation of the 'file_svg' formatter.
 *
 * @FieldFormatter(
 *   id = "file_svg",
 *   label = @Translation("SVG file"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class FileSVGFormatter extends FileFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $uri = $file->getFileUri();
      $absolute_url = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);
      $relative_url = \Drupal::service('file_url_generator')->generateString($uri);

      $xml = simplexml_load_file($absolute_url);
      $xml_attributes = $xml->attributes();
      $width = substr($xml_attributes->width, 0, -2);
      $height = substr($xml_attributes->height, 0, -2);

      $markup = '<a class="photoswipe" href="' . $relative_url . '" ';
      $markup .= 'data-size="' . $width .  'x' . $height . '">';
      $markup .= '<img src="' . $relative_url . '" /></a>';

      $elements[$delta] = array(
        '#markup' => $markup,
        '#cache' => array(
          'tags' => $file->getCacheTags(),
        ),
      );
    }

    return $elements;
  }

}
