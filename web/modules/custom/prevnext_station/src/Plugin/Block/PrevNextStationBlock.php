<?php

/**
 * @file
 * Contains \Drupal\prevnext_station\Plugin\Block\PrevNextStationBlock.
 */

namespace Drupal\prevnext_station\Plugin\Block;

use Drupal\Core\Link;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Url;


/**
 * Provides a block with previous and next stations links.
 *
 * @Block(
 *   id = "prevnext_station_block",
 *   admin_label = @Translation("Previous Next Station Block"),
 *   category = @Translation("Blocks")
 * )
 */
class PrevNextStationBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [];
    if ($node = \Drupal::routeMatch()->getParameter('node')) {
      $line = $node->field_line->getString();
      $order = $node->field_line_order->getString();

      $order_prev = [$order - 1];
      if (!$node->field_line_prev_alter->isEmpty()) {
        $order_prev[] = $node->field_line_prev_alter->getString();
      }

      $order_next = [$order + 1];
      if (!$node->field_line_next_alter->isEmpty()) {
        $order_next[] = $node->field_line_next_alter->getString();
      }

      $link_prev = [];
      $prev = $this->generateNextPrevious($line, $order_prev);
      if ($prev) {
        $link_prev = [
          '#prefix' => '<div class="nav-prev">',
          '#suffix' => '</div>',
          '#markup' => $prev,
        ];
      }

      $link_next = [];
      $next = $this->generateNextPrevious($line, $order_next);
      if ($next) {
        $link_next = [
          '#prefix' => '<div class="nav-next">',
          '#suffix' => '</div>',
          '#markup' => $next,
        ];
      }
      $render_service = \Drupal::service('renderer');
      $build['link'] = [
        '#markup' => $render_service->render($link_prev) . $render_service->render($link_next),
      ];
    }

    return $build;
  }

  public function getCacheTags() {
    // With this when your node change your block will rebuild.
    if ($node = \Drupal::routeMatch()->getParameter('node')) {
      // If there is node add its cache tag.
      return Cache::mergeTags(parent::getCacheTags(), array('node:' . $node->id()));
    } else {
      // Return default tags instead.
      return parent::getCacheTags();
    }
  }

  public function getCacheContexts() {
    // If you depends on \Drupal::routeMatch()
    // you must set context of this block with 'route' context tag.
    // Every new route this block will rebuild.
    return Cache::mergeContexts(parent::getCacheContexts(), array('route'));
  }

  /**
   * Lookup the next or previous node.
   */
  private function generateNextPrevious($line, $orders) {

    $query = \Drupal::entityQuery('node');
    $nids = $query->condition('type', 'station')
      ->accessCheck(TRUE)
      ->condition('field_line', $line)
      ->condition('field_line_order', $orders, 'IN')
      ->condition('status', 1)
      ->execute();

    if (!empty($nids)) {
      $nodes = \Drupal::service('entity_type.manager')->getStorage('node')->loadMultiple($nids);
      $output = '';
      foreach ($nodes as $node) {
        $title = $node->getTitle();
        $url = Url::fromRoute('entity.node.canonical', ['node' => $node->id()]);
        $output .= Link::fromTextAndUrl($title, $url)->toString();
      }
      return $output;
    }
    else {
      return;
    }
  }
}
