<?php

namespace Drupal\front_redirect\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Redirect front page.
 */
class FrontRedirectSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return([
      KernelEvents::REQUEST => [
        ['redirectFrontPage'],
      ],
    ]);
  }

  /**
   * Redirect requests for front page to /msk.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   Event object.
   */
  public function redirectFrontPage(RequestEvent $event): void {
    $request = $event->getRequest();

    // Only redirect the front page.
    if ($request->getRequestUri() !== '/') {
      return;
    }

    $response = new RedirectResponse('/msk', 301);
    $event->setResponse($response);
  }

}
